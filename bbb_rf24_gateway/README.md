# BBB gateway
## Routes that should be configured on a node where gateway is running

```
% echo 1 > /proc/sys/net/ipv4/ip_forward
% iptables -t nat -A PREROUTING -i tun_nrf24 -p tcp -m tcp --dport 1883 -j DNAT --to-destination 192.168.1.157:1883
% iptables -t nat -A POSTROUTING -o tun_nrf24 -j SNAT --to-source 10.10.3.13
% iptables -t nat -A POSTROUTING -j MASQUERADE
```

```
adaszek@adaszek-All-Series ~ $ mkfifo /tmp/remote
adaszek@adaszek-All-Series ~ $ ssh adaszek@192.168.1.180 "echo x345asd | sudo -S tcpdump -s 0 -U -n -w - -i tun_nrf24 not port 22" > /tmp/remote
```
