#/bin/bash

directory="./build"

function generateFreshMakefile() {
    echo "** Creating directory"
    mkdir $directory
    cd $directory
    echo "** Starting cmake"
    cmake -DCMAKE_TOOLCHAIN_FILE=../toolchain-arm-linux-gnueabihf.cmake ..
    echo "** Building"
    make
    echo "** Sending file to arm"
    scp server adaszek@arm:/home/adaszek/workspace
}

if [ ! -d $directory ];
then
    generateFreshMakefile
else
    rm -rf $directory
    generateFreshMakefile
fi

