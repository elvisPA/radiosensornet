#include <iostream>
#include <RF24/RF24.h>
#include "../definitions.h"

t_reading temperatureReading;

RF24 radio(115, 0);

void setup()
{
    radio.begin();
    radio.setPALevel(RF24_PA_MAX);
    radio.setChannel(120);
    radio.setDataRate(RF24_250KBPS);

    radio.openReadingPipe(1, pipes[0]);
    radio.startListening();
    radio.printDetails();

}

void loop()
{
    unsigned long startedWaitingAt = millis();
    bool timeout = false;
    while(!radio.available() && !timeout)
    {
        if(millis() - startedWaitingAt > 2000)
        {
            timeout = true;
        }
    }

    if(timeout)
    {
        std::cout << "Failed to receive anything" << std::endl;
    }
    else
    {
        while(radio.available())
        {
            radio.read(&temperatureReading, sizeof(t_reading));
        }
        std::cout << "{time: "
	              << temperatureReading.timeOfMeasurement
                  << ", temp: "
                  << temperatureReading.temp
                  << ", humidity: "
                  << temperatureReading.humi
                  << ", voltage "
                  << temperatureReading.voltage
                  << "}" << std::endl;

    }

    delay(925);
}

int main(int argc, char *argv[])
{
    setup();
    while(1)
    {
        loop();
    }
      
    return 0;
}
